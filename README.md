# The 2log Dot PCB

This repository contains the KiCad Project for the 2log Dot. The hardware is designed to be used with [2log Dot](https://github.com/2log-io/2log-dot "2log Dot Firmware")

## Parts

The PCB consists of a 4-Layer stack. All required parts are through hole components with the exception of the capacitor C1 which is a combined footprint.
Besides the PCB itself you need the following parts:
  * 24 Led WS2812B Ring with 4 contacts (diameter 66mm)
  * SMD Capacitor 5mm diameter, about 10uF
  * (alternative) THT Capacitor, about 10uF
  * Wemos D1 mini ESP32 (ESP8266 will not work)
  * PN532 RFID Reader V3 (red modules, sold under elechouse brand or clones)
  * mini pushbutton (the tiny black/silver ones)
  * _(optional) 14 Pin Male and 13 Pin Female 2.54mm Pin Header/Socket for expansion port_
  
## Renderings


![](/images/preview/2logDot_1.png)
![](/images/preview/2logDot_2.png)



## Assembling

When soldering the dot together, we must pay strict attention to the sequence in which we solder the individual components to the board. If you make a mistake here, you will quickly get into a dead-lock. I highly recommend using the template that can be clamped between the reader and PCB. This ensures that the LED sits above the PN532. This avoids ugly shadows. you can find the stl file here: https://gitlab.com/2log-io/hardware/2log-dot-case

It's a good idea to put the ESP loose on the pins first, while soldering the pins tight to the board. This ensures that the ESP can be plugged in later without any problems when it is time for its turn.

![](/images/assembling/IMG_1.jpeg)

Next, the button can be soldered on.

![](/images/assembling/IMG_2.jpeg)

Solder the pins to the PN532. Also here it makes sense to insert the pins loosely into the board during soldering. Otherwise it can happen that you solder the pins a little bit crooked. Then they no longer fit into the PCB and you have trouble.

![](/images/assembling/IMG_3.jpeg)

Now you can connect the reader to the board. In between comes the template that we printed out earlier with the 3D printer.  Make sure that the recess in the template is positioned so that you have free access to the pins for the LED ring.

![](/images/assembling/IMG_4.jpeg)

Now we can solder the capacitor and take the opportunity to cut off the protruding pins of the PN532. 

![](/images/assembling/IMG_5.jpeg)

Next, the ESP can be soldered.

![](/images/assembling/IMG_6.jpeg)

Now we can work on the LED ring. For this I took copper wire and stripped it. Next, the stranded wire is tinned and soldered to the contacts of the LED ring. 

![](/images/assembling/IMG_7.jpeg)

Bend the pins upwards and put the ring on the template. Here you have to thread the stranded wire through the holes of the board and solder it from the backside.

![](/images/assembling/IMG_8.jpeg)

Congratulations! You now have a 2log Dot. You can find a 3D printable case here: https://gitlab.com/2log-io/hardware/2log-dot-case


## The case

![](/images/assembling/IMG_9.jpeg)

![](/images/assembling/IMG_10.jpeg)
